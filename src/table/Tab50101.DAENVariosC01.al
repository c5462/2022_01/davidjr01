table 50101 "DAENVariosC01"
{
    Caption = 'Varios';
    LookupPageId = DAENListadeVariosC01;
    DrillDownPageId = DAENListadeVariosC01;
    DataClassification = ToBeClassified;

    fields
    {
        field(1; Tipo; Enum DAENVariosC01)
        {
            Caption = 'Tipo';
            DataClassification = SystemMetadata;
        }
        field(2; Codigo; Code[20])
        {
            Caption = 'Codigo';
            DataClassification = SystemMetadata;
        }
        field(3; Descripcion; Text[50])
        {
            Caption = 'Descripcion';
            DataClassification = SystemMetadata;
        }
        field(4; Bloqueado; Boolean)
        {
            Caption = 'Bloqueado';
            DataClassification = SystemMetadata;
        }

    }
    keys
    {
        key(PK; Tipo, Codigo, Descripcion)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {
        fieldgroup(DropDown; Codigo, Descripcion)
        {

        }
    }

    trigger OnDelete()
    begin
        rec.TestField(Bloqueado, true);

    end;
}
