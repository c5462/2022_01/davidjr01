table 50100 "DAENConfiguracionC01"
{
    Caption = 'Configuración de la app';
    DataClassification = OrganizationIdentifiableInformation;

    fields
    {
        field(1; Id; Code[10])
        {
            Caption = 'Id. de registro';
            DataClassification = SystemMetadata;
        }
        field(2; CodCteWeb; Code[20])
        {
            Caption = 'Código cliente para web';
            DataClassification = SystemMetadata;
            TableRelation = Customer."No.";
            ValidateTableRelation = true;
        }
        field(3; TextoResgistro; Text[250])
        {
            Caption = 'Texto resgistro';
            DataClassification = SystemMetadata;
        }
        field(4; TipoDoc; Enum DAENTipoDocC01)
        {
            Caption = 'Tipo documento';
            DataClassification = SystemMetadata;
        }
        field(5; NombreCliente; text[100])
        {
            Caption = 'Nombre Cliente';
            FieldClass = FlowField;
            CalcFormula = lookup(Customer.Name where("No." = field(CodCteWeb)));
            Editable = false;
        }
        field(6; TipoTabla; Enum DAENTipoTablaC01)
        {
            Caption = 'Tipo de Tabla';
            DataClassification = SystemMetadata;
            trigger OnValidate()
            begin
                if xRec.TipoTabla <> rec.TipoTabla then begin

                end;
                rec.Validate(CodTabla, '');
            end;
        }
        field(7; CodTabla; Code[20])
        {
            Caption = 'Código de tabla';
            DataClassification = SystemMetadata;
            TableRelation = if (TipoTabla = const(Cliente)) Customer else
            if (TipoTabla = const(Contacto)) Contact else
            if (TipoTabla = const(Empleado)) Employee else
            if (TipoTabla = const(Proovedor)) Vendor else
            if (TipoTabla = const(Recurso)) Resource;
        }
        field(8; ColorFondo; Enum DAENColoresC01)
        {
            Caption = 'ColorFondo';
            DataClassification = SystemMetadata;
        }
        field(9; ColorLetra; Enum DAENColoresC01)
        {
            Caption = 'ColorLetra';
            DataClassification = SystemMetadata;
        }

    }
    keys
    {
        key(PK; Id)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {

    }
    /// <summary>
    /// Recupera el registro de la tabla actual y si no existe lo crea.
    /// </summary>
    /// <returns>Return value of type Boolean.</returns>
    procedure GetF(): Boolean
    begin
        if not Rec.Get() then begin
            rec.Init();
            rec.Insert(true);
        end;
        exit(true);
    end;
    /// <summary>
    /// Devuelve el nombre del cliente pasado el parametro pCodCte. Si no existe devolvera un texto vacio.
    /// </summary>
    /// <param name="pCodCte"></param>
    /// <returns></returns>
    procedure NombreClienteF(pCodCte: Code[20]): Text
    var
        rlcustomer: Record Customer;
    BEGIN
        if rlcustomer.get(pCodCte) then begin
            exit(rlcustomer.Name);
        end;
    END;

    procedure NombreTablaF(pTipoTabla: Enum DAENTipoTablaC01; pCodTabla: Code[20]): Text
    var
        rlDAENConfiguracionC01: Record DAENConfiguracionC01;
        rlcontact: Record Contact;
        rlemployee: Record Contact;
        rlvendor: Record Contact;
        rlresource: Record Contact;
    begin
        case pTipoTabla of
            pTipoTabla::Cliente:
                begin
                    exit(rlDAENConfiguracionC01.NombreClienteF(pCodTabla))
                end;
            pTipoTabla::Contacto:
                begin
                    if rlcontact.get(pCodTabla) then begin
                        exit(rlcontact.Name)
                    end;
                end;
            pTipoTabla::Empleado:
                begin
                    if rlemployee.get(pCodTabla) then begin
                        exit(rlemployee.name)
                    end
                end;
            pTipoTabla::Proovedor:
                begin
                    if rlvendor.get(pCodTabla) then begin
                        exit(rlvendor.name)
                    end
                end;
            pTipoTabla::Recurso:
                begin
                    if rlresource.get(pCodTabla) then begin
                        exit(rlresource.name)
                    end
                end;
        end;
    end;

}