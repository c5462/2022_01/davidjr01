tableextension 50100 "DAENCustomerEXTC01" extends Customer
{
    fields
    {
        field(50100; DAENNumVacunasC01; Integer)
        {
            Caption = 'Nº de Vacunas';
            DataClassification = CustomerContent;
        }
        field(50101; DAENFechaUltimaVacunaC01; Date)
        {
            Caption = 'Fecha Última Vacuna';
            DataClassification = CustomerContent;
        }
        field(50102; DAENTipoVacunaC01; Code[20])
        {
            Caption = 'Tipo de Vacuna';
            DataClassification = CustomerContent;
            TableRelation = DAENVariosC01.Codigo where(Tipo = const(Vacuna));
            trigger OnValidate()
            var
                rlDAENVariosC01: Record DAENVariosC01;
            begin
                if rlDAENVariosC01.Get(rlDAENVariosC01.Tipo::Vacuna, Rec.DAENTipoVacunaC01) then begin
                    rlDAENVariosC01.TestField(Bloqueado, false);
                end;
            end;
        }

    }

}
