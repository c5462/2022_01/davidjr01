page 50100 "DAENConfiguracionC01"
{
    Caption = 'Configuracion de la app 01 del curso';
    PageType = Card;
    SourceTable = DAENConfiguracionC01;
    AboutTitle = 'En esta página crearemos la configuración de la primera app del curso de Especialistas de Bussiness Central 2022';
    AboutText = 'Configure la app de forma correcta. Asegurese que asigna el valor del campo del cliente web';
    AdditionalSearchTerms = 'Mi primera APP';
    ApplicationArea = ALL;
    UsageCategory = Administration;
    DeleteAllowed = false;
    InsertAllowed = false;

    layout
    {
        area(content)
        {
            group(General)
            {
                field(CodCteWeb; Rec.CodCteWeb)
                {
                    ToolTip = 'Especifique el cliente de BC que se utilizara para los pedidos que entren por web';
                    ApplicationArea = All;
                    AboutTitle = 'Campo Code. cliente web';
                    AboutText = 'Especifique el cliente de BC que se utilizara para los pedidos que entren por web';
                    Importance = Promoted;
                }
                field(TextoResgistro; Rec.TextoResgistro)
                {
                    ToolTip = 'Specifies the value of the Texto resgistro field.';
                    ApplicationArea = All;
                    AboutTitle = 'El Registro';
                    AboutText = 'Especifique el texto de configuracion que pasara a los movimientos de clientes';
                }
                field(TipoDoc; Rec.TipoDoc)
                {
                    ToolTip = 'Specifies the value of the Tipo documento field.';
                    ApplicationArea = All;
                }

                field(CodTabla; Rec.CodTabla)
                {
                    ToolTip = 'Specifies the value of the Tipo documento field.';
                    ApplicationArea = All;
                }
                field(NombreTabla; Rec.NombreTablaF(Rec.TipoTabla, rec.CodTabla))
                {
                    ToolTip = 'Specifies the value of the Tipo documento field.';
                    ApplicationArea = All;
                    Caption = 'Nombre Tabla';
                    Editable = false;
                }
                field(TipoTabla; Rec.TipoTabla)
                {
                    ToolTip = 'Specifies the value of the Tipo documento field.';
                    ApplicationArea = All;
                }
                field(ColorFondo; rec.ColorFondo)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Tipo documento field.';
                }
                field(ColorLetra; rec.ColorLetra)
                {
                    ApplicationArea = All;
                    ToolTip = 'Specifies the value of the Tipo documento field.';
                }
            }
            group(InfInterna)
            {
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                }
                field(SystemCreatedBy; Rec.SystemCreatedBy)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedBy field.';
                    ApplicationArea = All;
                }
                field(SystemId; Rec.SystemId)
                {
                    ToolTip = 'Specifies the value of the SystemId field.';
                    ApplicationArea = All;
                    Importance = Additional;
                }
                field(SystemModifiedAt; Rec.SystemModifiedAt)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedBy; Rec.SystemModifiedBy)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedBy field.';
                    ApplicationArea = All;
                }
            }

            group(CamposCalculados)
            {
                Caption = 'Campos Calculados';
                field(NombreCliente; Rec.NombreCliente)
                {
                    ToolTip = 'Nos muestra el nombre del cliente web';
                    ApplicationArea = All;
                }
                field(NombreClientePorFuncion; rec.NombreClienteF(rec.CodCteWeb))
                {
                    ApplicationArea = All;
                    Caption = 'Nombre Cliente';
                }

            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(Ejemplo01)
            {
                ApplicationArea = all;
                Caption = 'Mensaje de acción';
                Image = AboutNav;

                trigger OnAction()
                begin
                    Message('Acción pulsada');
                end;
            }
        }
    }
    trigger OnOpenPage()
    begin
        Rec.GetF();
    end;
}
