pageextension 50105 "DAENCustomerCardC01" extends "Customer Card"
{
    layout
    {
        addlast(content)
        {
            group(DAENVacunasC01)
            {
                Caption = 'Vacunas';

                field(DAENNumVacunasC01; Rec.DAENNumVacunasC01)
                {
                    ToolTip = 'Specifies the value of the Nº de Vacunas field.';
                    ApplicationArea = All;
                    Caption = 'Nº de Vacunas';
                }
                field(DAENFechaUltimaVacunaC01; Rec.DAENFechaUltimaVacunaC01)
                {
                    ToolTip = 'Specifies the value of the Fecha Última Vacuna field.';
                    ApplicationArea = All;
                    Caption = 'Fecha de ultima vacunación';
                }
                field(DAENTipoVacunaC01; Rec.DAENTipoVacunaC01)
                {
                    ToolTip = 'Specifies the value of the Tipo de Vacuna field.';
                    ApplicationArea = All;
                    Caption = 'Tipo de Vacuna';
                }
            }
        }
    }
    actions
    {
        addlast(processing)
        {
            action(DAENAsignarVacunaBloqueadaC01)
            {
                ApplicationArea = All;
                Caption = 'Asignar Vacuna Bloqueada';
                trigger OnAction()
                begin
                    Rec.Validate(DAENTipoVacunaC01, 'BLOQUEADA');
                end;
            }
        }
    }
}
