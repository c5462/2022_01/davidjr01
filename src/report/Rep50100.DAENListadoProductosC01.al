report 50100 "DAENListadoProductosC01"
{

    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = 'listadoproductos.rdlc';
    AdditionalSearchTerms = 'Listado Productos Ventas/Compras';
    Caption = 'Listado Productos Ventas/Compras';

    dataset
    {
        dataitem(Productos; Item)
        {
            RequestFilterFields = "No.", "Item Category Code";

            column(No_Productos; "No.")
            {
            }
            column(Description_Productos; Description)
            {
            }
            column(ItemCategoryCode_Productos; "Item Category Code")
            {
            }

            dataitem(FacturaVenta; "Sales Invoice Line")
            {
                DataItemLink = "No." = field("No.");
                RequestFilterFields = "Posting Date";

                column(Quantity_FacturaVenta; Quantity)
                {
                }
                column(UnitPrice_FacturaVenta; "Unit Price")
                {
                }
                column(AmountIncludingVAT_FacturaVenta; "Amount Including VAT")
                {
                }
                column(PostingDate_FacturaVenta; "Posting Date")
                {
                }
                column(NombreCliente; rcustomer.Name)
                {

                }
                column(NoCliente; rcustomer."No.")
                {

                }

                trigger OnPreDataItem()
                begin
                    FacturaVenta.SetRange("No.", Productos."No.");
                end;

                trigger OnAfterGetRecord()

                begin
                    if not rCustomer.Get(FacturaVenta."Sell-to Customer No.") then begin
                        Clear(rcustomer);
                    end;

                    ///rcustomer.SetRange("No.", FacturaVenta."Sell-to Customer No.");
                    /// rCustomer.SetFilter("No.",FacturaVenta."Sell-to Customer No." + '&' + FacturaVenta."Sell-to Customer No.");
                    ///rcustomer.FindFirst()
                end;

            }
            dataitem(FacturaCompra; "Purch. Inv. Line")
            {
                DataItemLink = "No." = field("No.");
                RequestFilterFields = "Posting Date";
                column(Quantity_FacturaCompra; Quantity)
                {
                }
                column(UnitCost_FacturaCompra; "Unit Cost")
                {
                }
                column(AmountIncludingVAT_FacturaCompra; "Amount Including VAT")
                {
                }
                column(PostingDate_FacturaCompra; "Posting Date")
                {
                }
                column(NombreProovedor; rvendor.Name)
                {

                }
                column(NoProovedor; rvendor."No.")
                {

                }
                trigger OnAfterGetRecord()

                begin
                    if not rvendor.get(FacturaCompra."Buy-from Vendor No.") then begin
                        Clear(rvendor);
                    end;
                end;
            }
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    field(Name; CompanyName)
                    {
                        ApplicationArea = All;

                    }
                }
            }
        }
    }
    var

        rcustomer: Record Customer;
        rvendor: Record Vendor;
}