report 50105 "DAENFacturaVenta1C01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    RDLCLayout = 'Facturaventa1.rdlc';
    DefaultLayout = RDLC;

    dataset
    {
        dataitem("Sales Invoice Header"; "Sales Invoice Header")
        {
            column(No_SalesInvoiceHeader; "No.")
            {

            }
            column(xCustAddr1; xCustAddr[1])
            {

            }
            column(xCustAddr2; xCustAddr[2])
            {

            }

            column(xShipAddr1; xShipAddr[1])
            {

            }
            column(xShipAddr2; xShipAddr[2])
            {

            }
            dataitem(Integer; Integer)
            {
                column(Number; Number)
                {

                }
                dataitem("Sales Invoice Line"; "Sales Invoice Line")
                {
                    //DataItemLink = "Document No." = field("No.");
                    column(LineNo_SalesInvoiceLine; "Line No.")
                    {

                    }
                    trigger OnPreDataItem()

                    begin
                        "Sales Invoice Line".SetRange("Document No.", "Sales Invoice Header"."No.");
                    end;

                }
                trigger OnPreDataItem()

                begin
                    Integer.SetRange(Number, 1, xcopias);
                end;

            }
            trigger OnAfterGetRecord()
            begin
                cuFormatAddress.SalesInvSellTo(xCustAddr, "Sales Invoice Header");
                cuFormatAddress.SalesInvShipTo(xCustAddr, xShipAddr, "Sales Invoice Header");
                rCompanyInfo.get;
                cuFormatAddress.Company(xCompanyAddr, rCompanyInfo);


            end;
        }


    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    field(xcopias; xcopias)
                    {
                        ApplicationArea = all;
                    }
                }


            }
        }
    }

    var
        xCustAddr: Array[8] of Text;
        xShipAddr: array[8] of Text;
        xCompanyAddr: array[8] of Text;
        rCompanyInfo: Record "Company Information";


        cuFormatAddress: codeunit "Format Address";


        xcopias: Integer;


}