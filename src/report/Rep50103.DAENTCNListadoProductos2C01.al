report 50103 "DAENTCNListadoProductos2C01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    RDLCLayout = 'listadoproyectos2.rdlc';
    DefaultLayout = RDLC;


    dataset
    {
        dataitem(DataItemName; Job)
        {
            //DataItemTableView = sorting("No.");
            RequestFilterFields = "No.", "Creation Date", Status;
            column(No_DataItemName; "No.")
            {

            }
            column(BilltoCustomerNo_DataItemName; "Bill-to Customer No.")
            {

            }
            column(BilltoName_DataItemName; "Bill-to Name")
            {

            }
            column(Description_DataItemName; "Description")
            {

            }
            column(CreationDate_DataItemName; "Creation Date")
            {

            }
            column(Status_DataItemName; Status)
            {

            }

            dataitem("Job Task"; "Job Task")
            {
                RequestFilterFields = "Job Task No.", "Job Task Type";
                DataItemLink = "Job No." = field("No.");

                column(JobTaskNo_JobTask; "Job Task No.")
                {

                }
                column(Description_JobTask; "Description")
                {

                }
                column(StartDate_JobTask; "Start Date")
                {

                }
                column(EndDate_JobTask; "End Date")
                {

                }
                column(JobTaskType_JobTask; "Job Task Type")
                {

                }
                column(xcolor_JobTask; "xcolor")
                {

                }



                dataitem("Job Planning Line"; "Job Planning Line")
                {
                    RequestFilterFields = "No.", "Planning Date", Type;
                    DataItemLink = "Job No." = field("Job No."), "Job Task No." = field("Job Task No.");
                    column(No_JobPlanningLine; "No.")
                    {

                    }
                    column(LineNo_JobPlanningLine; "Line No.")
                    {

                    }
                    column(Quantity_JobPlanningLine; Quantity)
                    {

                    }
                    column(Description_JobPlanningLine; Description)
                    {

                    }
                    column(PlanningDate_JobPlanningLine; "Planning Date")
                    {

                    }
                    column(LineAmount_JobPlanningLine; "Line Amount")
                    {

                    }
                    column(Type_JobPlanningLine; "Type")
                    {

                    }


                    dataitem("Job Ledger Entry"; "Job Ledger Entry")
                    {
                        DataItemLink = "Entry No." = field("Job Ledger Entry No.");
                        column(DocumentNo_JobLedgerEntry; "Document No.")
                        {

                        }
                        column(DocumentDate_JobLedgerEntry; "Document Date")
                        {

                        }
                        column(Type_JobLedgerEntry; "Type")
                        {

                        }
                        column(LineType_JobLedgerEntry; "Line Type")
                        {

                        }
                        column(EntryType_JobLedgerEntry; "Entry Type")
                        {

                        }
                        column(LedgerEntryType_JobLedgerEntry; "Ledger Entry Type")
                        {

                        }
                    }
                }
            }
            trigger OnAfterGetRecord()
            begin



                case DataItemName.Status of
                    DataItemName.Status::Completed:
                        begin
                            xcolor := 'Green'
                        end;
                    DataItemName.Status::Open:
                        begin
                            xcolor := 'Blue'
                        end;
                    DataItemName.Status::Planning:
                        begin
                            xcolor := 'Yellow'
                        end;
                    DataItemName.Status::Quote:
                        begin
                            xcolor := 'Red'
                        end;
                end;
            end;

        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    //field(Name; Source)
                    //{
                    // ApplicationArea = All;

                    // }
                }
            }
        }


    }

    var
        xcolor: Text;
}