enum 50102 "DAENColoresC01"
{
    Extensible = false;

    value(0; Blanco)
    {
        Caption = 'Blanco';
    }
    value(10; Rojo)
    {
        Caption = 'Rojo';
    }
    value(20; Gris)
    {
        Caption = 'Gris';
    }
    value(30; Negro)
    {
        Caption = 'Negro';
    }
}
