enum 50101 "DAENTipoTablaC01"
{
    Extensible = false;

    value(0; Cliente)
    {
        Caption = 'Tabla de cliente';
    }
    value(10; Proovedor)
    {
        Caption = 'Proovedor';
    }
    value(20; Recurso)
    {
        Caption = 'Recurso';
    }
    value(30; Empleado)
    {
        Caption = 'Empleado';
    }
    value(40; Contacto)
    {
        Caption = 'Contacto';
    }
}
