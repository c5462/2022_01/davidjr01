codeunit 50100 "DAENFuncionesC01"
{
    procedure NombreTablaF(pTipoTabla: Enum DAENTipoTablaC01; pCodTabla: Code[20]): Text
    var
        rlDAENConfiguracionC01: Record DAENConfiguracionC01;
        rlcontact: Record Contact;
        rlemployee: Record Contact;
        rlvendor: Record Contact;
        rlresource: Record Contact;
    begin
        case pTipoTabla of
            pTipoTabla::Cliente:
                begin
                    exit(rlDAENConfiguracionC01.NombreClienteF(pCodTabla))
                end;
            pTipoTabla::Contacto:
                begin
                    if rlcontact.get(pCodTabla) then begin
                        exit(rlcontact.Name)
                    end;
                end;
            pTipoTabla::Empleado:
                begin
                    if rlemployee.get(pCodTabla) then begin
                        exit(rlemployee.name)
                    end
                end;
            pTipoTabla::Proovedor:
                begin
                    if rlvendor.get(pCodTabla) then begin
                        exit(rlvendor.name)
                    end
                end;
            pTipoTabla::Recurso:
                begin
                    if rlresource.get(pCodTabla) then begin
                        exit(rlresource.name)
                    end
                end;
        end;
    end;
}
